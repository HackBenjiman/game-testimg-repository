﻿using UnityEngine;
using System.Collections;

public class Venom_Movement : MonoBehaviour
{
    public Rigidbody2D rb;
    public float speed;
    public float jumpPower;
    public bool isGrounded;
    public LayerMask isGroundLayer;
    public Transform groundCheck;
    public bool isFacingRight;
    public LayerMask isMaskLayer;
    Animator anim;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	    if (!rb)
        {
            Debug.LogError("No rigid2d found on character.");
        }

        if (speed == 0)
        {
            Debug.LogWarning("Speed not set in inspector. Defaulting to 5.");
            speed = 5.0f;
        }

        if (jumpPower == 0)
        {
            Debug.LogWarning("Jump force not set in inspector. Defaulting to 5.");
            jumpPower = 5.0f;
        }

        anim = GetComponent<Animator>();
        if (!anim)
        {
            Debug.LogError("No animator found on character.");
        }
	}
	
	void Update ()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, 0.2f, isGroundLayer);
        float moveValue = Input.GetAxisRaw("Horizontal");

        if (Input.GetButtonDown("Jump") && isGrounded)
        {
            Debug.Log("Jump.");
            rb.AddForce(Vector2.up * jumpPower, ForceMode2D.Impulse);
        }

        rb.velocity = new Vector2(moveValue * speed, rb.velocity.y);
        anim.SetFloat("Speed", Mathf.Abs(moveValue));

        if ((isFacingRight && moveValue < 0) || (!isFacingRight && moveValue > 0))
            flipVenom();
	}

    void flipVenom()
    {
        isFacingRight = !isFacingRight;
        Vector3 scaleFactor = transform.localScale;
        scaleFactor.x *= -1;
        transform.localScale = scaleFactor;
    }
}
